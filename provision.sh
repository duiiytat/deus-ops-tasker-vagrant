apt-get update
apt-get install ca-certificates curl gnupg lsb-release
mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
apt-get update
apt-get install -y gitlab-runner docker-ce docker-ce-cli containerd.io docker-compose-plugin
gitlab-runner register -n --url https://gitlab.com/ --registration-token $GITLAB_TOKEN --executor shell --description "vagrant-shell-runner" --docker-image "docker:dind" --tag-list "shell-runner-1"
gitlab-runner register -n --url https://gitlab.com/ --registration-token $GITLAB_TOKEN --executor docker --description "vagrant-docker-runner" --docker-image "docker:dind" --tag-list "docker-runner-1"
